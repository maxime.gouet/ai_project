# Projet IA

## Description du système multi-agents

Notre système multi-agents est constitué des agents suivants:

- Planète
- Robot centralisateur 
- Robots extracteurs
- Robots Agriculteurs
- Robots Constructeurs
- Robots Récolteurs

### Environnement

Les robots évoluent dans l'environnement qui est la planète. 
Quant à celle-ci elle ne fait pas vraiment partie d'un environnement étant donné que c'est elle l'environnement pour les robots. 
On pourrait néanmoins imaginer que la planète évolue avec l'univers comme environnement, mais elle n'a pas d'interaction avec celui-ci.

### Interactions et coopération entre agents

#### Planète

La planète n'a pas d'interaction directe avec les robots, néanmoins elle réagit à leur activité (voir fonctionnement des agents).
Ce qui peut provoquer la destruction de construction des robots voir les tuer.

#### Robots

Tous les robots communique entre eux pour mettre a jours en temps réelle leurs connaissance de la map
lors d'un deplacement il mette a jour leur carte interne et l'envoie au centralisateur qui va ensuite
l'envoyer aux autres robots.
Nos agents coopére seulement pour le partage d'informations.

### Objectifs des agents

#### Planète

L'objectif de la planète est de vivre sa vie paisiblement. 
Mais elle va perturber ses habitants si ceux-ci ne respectent pas leur habitat.

#### Robots

L'objectif des robots est d'explorer la planète pour exploiter ses ressources (nourriture, minerai, eau..). 
Ces dernières sont acheminées puis stockées à la base.
Leurs objectif dépend de leurs type , c'est vu deffinire des intéret différents
Extracteur , interet sur les minerai
Constructeur , interet sur les points d'eau ...


### Fonctionnement des agents (perceptions, actions, ...)

#### Planète

La planète est un agent réactif. 
Elle connait les différentes cases qui la compose (pierre, forêt, ...) ainsi que le taux d'extraction des minerais et l'eau.
Elle réagit via des métamorphoses de son terrain proportionnellement à l'extraction de ses ressources primaires.

#### Robots

Les Robots fonctionne via un algorithme de Q learning , qui se calcule en fonction de la connaisance actuelle de la map du robot
Le Qlearning va ensuite valoriser différament les case en fonction du type de robot.
Une fois sur la case d'interet le QLearning laisse sa place a l'algo A* pour le retour a la base
Pour la perception les robots persoivent les 4 case autour d'eux , et partage ensuite ces nouvels informations
avec le restes des robots via le mecanisme de connaissance partagé par les robots.
Le retour s'effectue en chercheant le meilleur chemin en utilisant une heuristique qui permet d'estimer la distance pour aller à la base en prenant en compte le coût des cases. (exemple: le déplacement sur une case d'eau peut être plus lent)
