package core.view;

import core.MainSketch;
import processing.core.PApplet;
import processing.core.PImage;

import java.util.HashMap;

public class RobotTileset {
  private final HashMap<Character, PImage> tiles;

  public RobotTileset(String path, int tileSize) {
    PApplet sketch = MainSketch.getInstance();

    PImage tileset = sketch.loadImage(path);
    tiles = new HashMap<>();

    tiles.put('A', tileset.get(0, 0, tileSize, tileSize));
    tiles.put('B', tileset.get(tileSize, 0, tileSize, tileSize));
    tiles.put('C', tileset.get(tileSize*2, 0, tileSize, tileSize));
    tiles.put('E', tileset.get(tileSize*3, 0, tileSize, tileSize));
    tiles.put('R', tileset.get(tileSize*4, 0, tileSize, tileSize));
  }

  public PImage getTile(char c) {
    return tiles.get(c);
  }
}
