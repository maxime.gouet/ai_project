package core.view;

import core.MainSketch;
import processing.core.PApplet;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class Popover {
  private final ArrayList<String> text;
  private Point position;
  private float textWidth;
  private float textHeight;
  private boolean visible;

  public Popover(String _text, Point _position) {
    text = new ArrayList<>();
    text.add(_text);
    position = _position;
    visible = true;
  }

  public void display() {
    if (visible) {
      PApplet sketch = MainSketch.getInstance();

      sketch.noStroke();
      sketch.fill(100, 100, 100, 240);
      sketch.rect(position.x + 10, position.y, textWidth, textHeight);

      sketch.fill(0, 0, 0);
      sketch.textSize(14);
      for (int i = 0; i < text.size(); i++) {
        sketch.text(text.get(i), position.x + 10, position.y + 12 + (i * 14));
      }
    }
  }

  public void setPosition(Point _position) {
    position = _position;
  }

  public void setVisible(boolean _visible) {
    visible = _visible;
  }

  public void setText(String _text) {
    PApplet sketch = MainSketch.getInstance();
    text.clear();

    String[] arr = _text.split("\n");
    text.addAll(Arrays.asList(arr));
    textWidth = sketch.textWidth(text.stream().max(Comparator.comparing(String::length)).get());
    textHeight = 15 * text.size();
  }
}
