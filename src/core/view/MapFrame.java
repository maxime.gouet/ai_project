package core.view;

import core.MainSketch;
import core.planet.map.Map;
import core.planet.map.tiles.TLake;
import core.planet.map.tiles.Tile;
import processing.core.PApplet;
import processing.core.PImage;

import java.awt.*;

public class MapFrame {
    private final MapTileset mapTileset;
    private final Tile[][] map;

    private final Popover popover;

    public MapFrame(Tile[][] _map) {
        map = _map;
        mapTileset = new MapTileset("resources/Tilesets.png", 59);
        popover = new Popover("", new Point(0, 0));
    }

    public void display(PApplet sketch) {
        int TILE_SIZE = MainSketch.getTileSize();

        for (int row = 0; row < 21; row++) {
            for (int col = 0; col < 21; col++) {
                Tile tile = map[row][col];

                PImage sprite = mapTileset.getTile(tile.getName());
                sprite.resize(TILE_SIZE, TILE_SIZE);

                sketch.image(sprite, TILE_SIZE * row, TILE_SIZE * col);

                if (tile.HasAPipeline()) {
                    sketch.fill(212, 7, 78);
                    sketch.noStroke();
                    sketch.rect((TILE_SIZE * row) + (TILE_SIZE / 2) - 6, (TILE_SIZE * col) + (TILE_SIZE / 2) - 6, 12, 12);
                    if (tile instanceof TLake && ((TLake) tile).isSource()) {
                        if (((TLake)tile).isFullyConnected()) {
                            sketch.fill(0, 255, 255);
                            sketch.rect((TILE_SIZE * row) + (TILE_SIZE / 2) - 2, (TILE_SIZE * col) + (TILE_SIZE / 2) - 2, 4, 4);
                        }
                        else {
                            sketch.fill(102, 51, 0);
                            sketch.rect((TILE_SIZE * row) + (TILE_SIZE / 2) - 2, (TILE_SIZE * col) + (TILE_SIZE / 2) - 2, 4, 4);
                        }
                    }
                }
            }
        }

        popover.display();
    }

    public int frameWidth() {
        return 21 * MainSketch.getTileSize();
    }

    public int frameHeight() {
        return 21 * MainSketch.getTileSize();
    }
}
