package core.view;

import core.MainSketch;
import core.Simulation;
import core.Utils;
import processing.core.PApplet;

public class InformationFrame {
  private final int relativeX;
  private final Simulation simulation;

  public InformationFrame(int _relativeX, Simulation _simulation) {
    relativeX = _relativeX;
    simulation = _simulation;
  }

  public void display(PApplet sketch) {
    int TILE_SIZE = MainSketch.getTileSize();

    sketch.stroke(0, 0, 0);
    sketch.strokeWeight(3);
    sketch.line(21 * TILE_SIZE, 0, 21 * TILE_SIZE, sketch.height);
    sketch.line(21 * TILE_SIZE, 0, sketch.width, 0);
    sketch.line(sketch.width - 1, 0, sketch.width - 1, sketch.height);
    sketch.line(21 * TILE_SIZE, sketch.height - 1, sketch.width, sketch.height - 1);

    sketch.fill(0, 102, 153);

    sketch.textSize(14);
    sketch.text("Rounds : " + simulation.getRounds(), relativeX + 5, 20);
    sketch.text("Ore mined : " + simulation.getPlanet().percentageOfOreMined() + " %", relativeX + 5, 40);
    sketch.text("Water extracted : " + Utils.rateFormatting(simulation.getPlanet().percentageOfWaterExtracted()) + " %", relativeX + 5, 60);
    sketch.text("Metamorphose rate : " + Utils.rateFormatting((float)simulation.getPlanet().getMetamorphoseRate()) + " %", relativeX + 5, 80);
  }
}
