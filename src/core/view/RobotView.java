package core.view;

import core.MainSketch;
import core.robots.Robot;
import processing.core.PApplet;
import processing.core.PImage;

import java.util.ArrayList;

public class RobotView {
    private final ArrayList<Robot> robots;
    private final RobotTileset tileset;

    public RobotView(ArrayList<Robot> robots) {
        this.robots = robots;
        tileset = new RobotTileset("resources/robots.png", 16);
    }

    public void display(PApplet sketch) {
        int TILE_SIZE = MainSketch.getTileSize();

        sketch.noStroke();
        for (Robot robot : robots) {
            sketch.fill(0, 0, 255);
            PImage sprite = tileset.getTile(getRobotName(robot));
            sprite.resize(TILE_SIZE, TILE_SIZE);
            sketch.image(sprite, robot.getPosition().x * TILE_SIZE, robot.getPosition().y * TILE_SIZE);
        }
    }

    private char getRobotName(Robot robot) {
        return switch (robot.getClass().getName()) {
            case "core.robots.Centralisateur" -> 'B';
            case "core.robots.Constructeur" -> 'C';
            case "core.robots.Extracteur" -> 'E';
            case "core.robots.Recolteur" -> 'R';
            default -> 'A';
        };
    }
}
