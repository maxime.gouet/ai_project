package core.view;

import core.MainSketch;
import processing.core.PApplet;
import processing.core.PImage;

import java.util.HashMap;

public class MapTileset {
  private final HashMap<Character, PImage> tiles;

  public MapTileset(String path, int tileSize) {
    PApplet sketch = MainSketch.getInstance();

    PImage tileset = sketch.loadImage(path);
    tiles = new HashMap<>();

    tiles.put('B', tileset.get(0, 0, tileSize, tileSize));
    tiles.put('Z', tileset.get(tileSize, 0, tileSize, tileSize));
    tiles.put('N', tileset.get(tileSize*2, 0, tileSize, tileSize));
    tiles.put('D', tileset.get(tileSize*3, 0, tileSize, tileSize));
    tiles.put('L', tileset.get(tileSize*4, 0, tileSize, tileSize));
    tiles.put('S', tileset.get(tileSize*5, 0, tileSize, tileSize));
    tiles.put('H', tileset.get(tileSize*6, 0, tileSize, tileSize));
    tiles.put('G', tileset.get(tileSize*7, 0, tileSize, tileSize));
    tiles.put('F', tileset.get(tileSize*8, 0, tileSize, tileSize)); // Need to jump the second tree
    tiles.put('M', tileset.get(tileSize*10, 0, tileSize, tileSize));
    tiles.put('P', tileset.get(tileSize*11, 0, tileSize, tileSize));
    tiles.put('U', tileset.get(tileSize*12, 0, tileSize, tileSize));
  }

  public PImage getTile(char c) {
    return tiles.get(c);
  }
}
