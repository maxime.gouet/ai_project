package core.graph;

public class Edge {

    public Node source;
    public Node destination;
    public double cost;

    public Edge(Node source, Node destination, double cost) {

        this.source = source;
        this.destination = destination;
        this.cost = cost;

    }
}
