package core.deplacement;

import java.awt.*;
import core.planet.map.tiles.Tile;
import core.robots.Type_robot;
import core.robots.WorkChoice;

/** Ce fichier à était fait par Maxime Gouet*/
public interface Strategie_deplacement {
    public Point mooving(Tile[][] map,int[][] exploration_value, Point pos, Type_robot type_bot, WorkChoice workChoice);
}
