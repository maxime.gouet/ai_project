/* author: Lucas Guilbert */
package core.deplacement;

import core.planet.map.tiles.TLake;
import core.planet.map.tiles.TObstacle;
import core.planet.map.tiles.Tile;
import core.robots.Type_robot;
import core.robots.WorkChoice;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;

// The algorithm bellow is based on the wikipedia one : https://en.wikipedia.org/wiki/A*_search_algorithm
// But in more Object oriented way.
public class A_star implements Strategie_deplacement {
    public class Node {
        public int x, y;
        private float f, g, h;
        private final ArrayList<Node> neighbors;
        public Node previous;
        private final boolean obstacle;

        private Node(int _x, int _y, boolean isObstacle) {
            x = _x;
            y = _y;
            f = 0;
            g = 0;
            h = 0;
            neighbors = new ArrayList<>();
            previous = null;
            obstacle = isObstacle;
        }

        private void addNeighbors(Node[][] grid) {
            if (x < 20) neighbors.add(grid[x + 1][y]);
            if (x > 0) neighbors.add(grid[x - 1][y]);
            if (y < 20) neighbors.add(grid[x][y + 1]);
            if (y > 0) neighbors.add(grid[x][y - 1]);
        }

        public Point getPosition() {
            return new Point(x, y);
        }
    }

    /* ALGORITHM RELATED THINGS ***************************************************************************************/
    private final Node[][] grid = new Node[21][21];
    // List of node that have not been evaluated
    private final ArrayList<Node> openSet = new ArrayList<>();
    // List of nodes that have been evaluated
    private final ArrayList<Node> closedSet = new ArrayList<>();
    private Node start;
    private Node end;
    private LinkedList<Node> path;

    private void init(Tile[][] map, Point pos, Type_robot type_robot) {
        path = new LinkedList<>();

        // Define all the nodes
        for (int col = 0; col < 21; col++) {
            for (int row = 0; row < 21; row++) {
                boolean isObstacle = map[row][col] instanceof TObstacle;
                if (type_robot != Type_robot.Constructeur)
                    isObstacle = isObstacle || map[row][col] instanceof TLake;
                grid[row][col] = new Node(row, col, isObstacle);
            }
        }

        // Retrieve the neighbors of each node
        for (int col = 0; col < 21; col++) {
            for (int row = 0; row < 21; row++) {
                grid[row][col].addNeighbors(grid);
            }
        }

        // Define the start position to be the position of the robot
        start = grid[pos.x][pos.y];
        // End position is where the Base is.
        end = grid[10][10];

        // The first node to check is the start position
        openSet.add(start);
    }

    private Point process() {
        while (openSet.size() > 0) {
            // First we are looking to the next node with the lowest f value
            int lowestIndex = 0;
            for (int i = 0; i < openSet.size(); i++) {
                if (openSet.get(i).f < openSet.get(lowestIndex).f) {
                    lowestIndex = i;
                }
            }
            Node current = openSet.get(lowestIndex);

            // If so we are done, so i go backward to retrieve the first node where my robot needs to go to.
            if (current.equals(end)) {
                path.add(end);
                Node node = end;
                while (node.previous != start) {
                    path.add(node.previous);
                    node = node.previous;
                }
                return new Point(node.x, node.y);
            }

            // Update lists
            openSet.remove(lowestIndex);
            closedSet.add(current);

            for (Node neighbor : current.neighbors) {
                // I don't want to reevaluate a Node that have been previously visited or that is an obstacle
                if (closedSet.contains(neighbor) || neighbor.obstacle) continue;

                float tempG = current.g + 1;

                // Maybe we have already visited this neighbor but the g i am currently working with is better (mean lower) that the previous one.
                // If that's the case it means that there is a better path (mean lower) to reach the neighbor that we previously thought.
                if (openSet.contains(neighbor) && tempG > neighbor.g) continue;

                neighbor.g = tempG;
                if (!openSet.contains(neighbor)) openSet.add(neighbor);

                neighbor.h = getHeuristic(neighbor);
                neighbor.f = neighbor.g + neighbor.h;
                neighbor.previous = current;
            }
        }

        // No solution founded
        return null;
    }

    // The heuristic I use is simply the distance between node and end position in a grid.
    private float getHeuristic(Node node) {
        return Math.abs(node.x - end.x) + Math.abs(node.y - end.y);
    }

    public LinkedList<Node> getPath() {
        return path;
    }

    public Point mooving(Tile[][] map, int[][] exploration_value, Point pos, Type_robot type_bot, WorkChoice workChoice) {
        init(map, pos, type_bot);
        if (start.equals(end)) {
            return pos;
        }

        return process();
    }
}
