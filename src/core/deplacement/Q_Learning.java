package core.deplacement;

import core.planet.map.tiles.TLake;
import core.planet.map.tiles.Tile;
import core.planet.map.tiles.TType;
import core.robots.Type_robot;
import core.robots.WorkChoice;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

/** Ce fichier à était fait par Maxime Gouet
 * En s'inspirant de l'algorithme donnée : http://technobium.com/reinforcement-learning-q-learning-java/
 * Pour plus de details concernant les details d'implementation du Q_learning et les choix effectuée voir
 * le rapport dans la portie dedié.
 */

public class Q_Learning implements Strategie_deplacement {

    private final double alpha = 0.1; // Learning rate
    private final double gamma = 0.9; // Eagerness - 0 looks in the near future, 1 looks in the distant future

    private final int mapWidth = 21;
    private final int mapHeight = 21;
    private final int statesCount = mapHeight * mapWidth;

    private char[][] map;  // Maze read from file
    private int[][] R;       // Reward lookup
    private double[][] Q;    // Q learning

    private final int reward = 100;
    private final int penalty = -1000;

    @Override
    public Point mooving(Tile[][] tile_map, int[][] exploration_value, Point pos, Type_robot type_bot, WorkChoice workChoice) {
        if(workChoice == WorkChoice.EXPLOITATION){
            if(!this.init(tile_map, pos, type_bot)){
                workChoice = WorkChoice.EXPLORATION;
                this.initExplo(tile_map,exploration_value, pos, type_bot);
            }
        }else{
            this.initExplo(tile_map,exploration_value, pos, type_bot);
        }

        this.calculateQ(workChoice);
        //printPolicy();
        int id_ar = getPolicyFromState(((pos.y) + ((pos.x) * 21)));
        return new Point((int) (id_ar / 21), id_ar % 21);
    }
    public boolean init(Tile[][] tile_map,Point pos,Type_robot type_bot){

        R = new int[statesCount][statesCount];
        Q = new double[statesCount][statesCount];
        map = new char[mapHeight][mapWidth];

        int x = 0;
        int y = 0;
        boolean existebjective = false;

        for (x = 0; x < mapHeight; x++) {
            for (y = 0; y < mapWidth; y++) {
                if (tile_map[x][y].getTType() == TType.Obstacle) {
                    map[x][y] = 'X';
                }
                if (tile_map[x][y].getTType() == TType.Food) {
                    if (type_bot == Type_robot.Recolteur) {
                        map[x][y] = 'F';
                        existebjective = true;
                    } else {
                        map[x][y] = '0';
                    }
                }
                if (tile_map[x][y].getTType() == TType.Ore) {
                    if (type_bot == Type_robot.Extracteur) {
                        map[x][y] = 'F';
                        existebjective = true;
                    } else {
                        map[x][y] = '0';
                    }
                }
                if (tile_map[x][y].getTType() == TType.Lake ) {
                    if (type_bot == Type_robot.Constructeur) {
                        if(!((TLake)tile_map[x][y]).isFullyConnected()){
                            map[x][y] = 'F';
                            existebjective = true;
                        }
                    } else {
                        map[x][y] = 'X';
                    }
                }
                if (tile_map[x][y].getTType() == TType.DriedMeadow || tile_map[x][y].getTType() == TType.FattishMeadow || tile_map[x][y].getTType() == TType.NormalMeadow) {
                    if (type_bot == Type_robot.Agriculteur) {
                        map[x][y] = 'F';
                        existebjective = true;
                    } else {
                        map[x][y] = '0';
                    }
                }
                if (tile_map[x][y].getTType() == TType.Base || tile_map[x][y].getTType() == TType.Desert || tile_map[x][y].getTType() == TType.Stone || tile_map[x][y].getTType() == TType.Forest) {
                    map[x][y] = '0';
                }
            }
        }

        if (!existebjective){
            return existebjective;
        }


        for (int k = 0; k < statesCount; k++) {

            // We will navigate with i and j through the maze, so we need
            // to translate k into i and j
            int i = k / mapWidth;
            int j = k - i * mapWidth;

            // Fill in the reward matrix with -1
            for (int s = 0; s < statesCount; s++) {
                R[k][s] = -1;
            }

            // If not in final state or a wall try moving in all directions in the maze
            if (map[i][j] != 'F') {

                // Try to move left in the map
                int goLeft = j - 1;
                if (goLeft >= 0) {
                    int target = i * mapWidth + goLeft;
                    if (map[i][goLeft] == '0') {
                        R[k][target] = 0;
                    } else if (map[i][goLeft] == 'F') {
                        R[k][target] = reward;
                    } else {
                        R[k][target] = penalty;
                    }
                }

                // Try to move right in the map
                int goRight = j + 1;
                if (goRight < mapWidth) {
                    int target = i * mapWidth + goRight;
                    if (map[i][goRight] == '0') {
                        R[k][target] = 0;
                    } else if (map[i][goRight] == 'F') {
                        R[k][target] = reward;
                    } else {
                        R[k][target] = penalty;
                    }
                }

                // Try to move up in the map
                int goUp = i - 1;
                if (goUp >= 0) {
                    int target = goUp * mapWidth + j;
                    if (map[goUp][j] == '0') {
                        R[k][target] = 0;
                    } else if (map[goUp][j] == 'F') {
                        R[k][target] = reward;
                    } else {
                        R[k][target] = penalty;
                    }
                }

                // Try to move down in the map
                int goDown = i + 1;
                if (goDown < mapHeight) {
                    int target = goDown * mapWidth + j;
                    if (map[goDown][j] == '0') {
                        R[k][target] = 0;
                    } else if (map[goDown][j] == 'F') {
                        R[k][target] = reward;
                    } else {
                        R[k][target] = penalty;
                    }
                }
            }
        }
        initializeQ();
        return existebjective;
    }

    public void initExplo(Tile[][] tile_map,int[][] exploration_value,Point pos,Type_robot type_bot){

        R = new int[statesCount][statesCount];
        Q = new double[statesCount][statesCount];
        map = new char[mapHeight][mapWidth];

        int x = 0;
        int y = 0;

        for (x = 0; x < mapHeight; x++) {
            for (y = 0; y < mapWidth; y++) {
                if (tile_map[x][y].getTType() == TType.Obstacle) {
                    map[x][y] = 'X';
                }
                if (tile_map[x][y].getTType() == TType.Lake) {
                    if (type_bot == Type_robot.Constructeur) {
                        map[x][y] = (char) exploration_value[x][y];
                    } else {
                        map[x][y] = 'X';
                    }
                }if (tile_map[x][y].getTType() != TType.Lake && tile_map[x][y].getTType() != TType.Obstacle){
                    map[x][y] = (char) exploration_value[x][y];
                }
            }
        }


        for (int k = 0; k < statesCount; k++) {

            // We will navigate with i and j through the maze, so we need
            // to translate k into i and j
            int i = k / mapWidth;
            int j = k - i * mapWidth;

            // Fill in the reward matrix with -1
            for (int s = 0; s < statesCount; s++) {
                R[k][s] = -1;
            }
            // Try to move left in the map
            int goLeft = j - 1;
            if (goLeft >= 0) {
                int target = i * mapWidth + goLeft;
                if (map[i][goLeft] == 'X') {
                    R[k][target] = penalty;
                } else {
                    R[k][target] = map[i][goLeft];
                }
            }

            // Try to move right in the map
            int goRight = j + 1;
            if (goRight < mapWidth) {
                int target = i * mapWidth + goRight;
                if (map[i][goRight] == 'X') {
                    R[k][target] = penalty;
                } else {
                    R[k][target] = map[i][goRight];
                }
            }

            // Try to move up in the map
            int goUp = i - 1;
            if (goUp >= 0) {
                int target = goUp * mapWidth + j;
                if (map[goUp][j] == 'X') {
                    R[k][target] = penalty;
                } else {
                    R[k][target] = map[goUp][j];
                }
            }

            // Try to move down in the map
            int goDown = i + 1;
            if (goDown < mapHeight) {
                int target = goDown * mapWidth + j;
                if (map[goDown][j] == 'X') {
                    R[k][target] = penalty;
                } else {
                    R[k][target] = map[goDown][j];
                }
            }
        }
        //printR(R);
        initializeQ();
    }

    void initializeQ() {
        for (int i = 0; i < statesCount; i++) {
            for (int j = 0; j < statesCount; j++) {
                Q[i][j] = (double) R[i][j];
            }
        }
    }

    void printR(int[][] matrix) {

        System.out.printf("%25s", "States: ");
        for (int i = 0; i <= 8; i++) {
            System.out.printf("%4s", i);
        }
        System.out.println();

        for (int i = 0; i < statesCount; i++) {
            System.out.print("Possible states from " + i + " :[");
            for (int j = 0; j < statesCount; j++) {
                System.out.printf("%4s", matrix[i][j]);
            }
            System.out.println("]");
        }
    }

    void calculateQ(WorkChoice workChoice) {
        Random rand = new Random();

        for (int i = 0; i < 100; i++) { // Train cycles
            // Select random initial state
            int crtState = rand.nextInt(statesCount);

            while (!isFinalState(crtState,workChoice)) {
                int[] actionsFromCurrentState = possibleActionsFromState(crtState);

                // Pick a random action from the ones possible
                int index = rand.nextInt(actionsFromCurrentState.length);
                int nextState = actionsFromCurrentState[index];

                // Q(state,action)= Q(state,action) + alpha * (R(state,action) + gamma * Max(next state, all actions) - Q(state,action))
                double q = Q[crtState][nextState];
                double maxQ = maxQ(nextState);
                int r = R[crtState][nextState];

                double value = q + alpha * (r + gamma * maxQ - q);
                Q[crtState][nextState] = value;

                crtState = nextState;
            }
        }
    }

    boolean isFinalState(int state,WorkChoice workChoice) {
        int i = state / mapWidth;
        int j = state - i * mapWidth;

        if(workChoice == WorkChoice.EXPLOITATION){
            return map[i][j] == 'F';
        }else{
            return map[i][j] >= 100 && map[i][j] != 'X';
        }
    }

    int[] possibleActionsFromState(int state) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < statesCount; i++) {
            if (R[state][i] != -1) {
                result.add(i);
            }
        }

        return result.stream().mapToInt(i -> i).toArray();
    }

    double maxQ(int nextState) {
        int[] actionsFromState = possibleActionsFromState(nextState);
        //the learning rate and eagerness will keep the W value above the lowest reward
        double maxValue = -10;
        for (int nextAction : actionsFromState) {
            double value = Q[nextState][nextAction];

            if (value > maxValue)
                maxValue = value;
        }
        return maxValue;
    }

    void printPolicy() {
        System.out.println("\nPrint policy");
        for (int i = 0; i < statesCount; i++) {
            System.out.println("From state " + i + " goto state " + getPolicyFromState(i));
        }
    }

    int getPolicyFromState(int state) {
        int[] actionsFromState = possibleActionsFromState(state);

        double maxValue = Double.MIN_VALUE;
        int policyGotoState = state;

        // Pick to move to the state that has the maximum Q value
        for (int nextState : actionsFromState) {
            double value = Q[state][nextState];

            if (value > maxValue) {
                maxValue = value;
                policyGotoState = nextState;
            }
        }
        return policyGotoState;
    }

    void printQ() {
        for (int i = 0; i < Q.length; i++) {
            System.out.print("From state " + i + ":  ");
            for (int j = 0; j < Q[i].length; j++) {
                System.out.printf("%6.2f ", (Q[i][j]));
            }
            System.out.println();
        }
    }

}
