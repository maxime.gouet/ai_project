/* author: Alexis Mairena */

package core.deplacement;

import core.graph.*;
import core.planet.map.tiles.Tile;
import core.robots.Type_robot;
import core.robots.WorkChoice;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class A_star2 implements Strategie_deplacement {

    Tile startNode;
    Tile endNode;

    ArrayList<Node> listNodes = null;
    ArrayList<Edge> listEdges = null;

    public Point mooving(Tile[][] tile_map, int[][] exploration_value, Point pos, Type_robot type_bot, WorkChoice workChoice) {

        scanEnv(tile_map, pos);
        pos = Resolve(tile_map);

        return pos;                                                                      // New position provided by Astar.

    }

    public void scanEnv(Tile[][] tile_map, Point pos) {

        startNode = tile_map[pos.x][pos.y];                                              // Departure is at the current robot position.
        startNode.distanceFromStart = startNode.getCost();                               // Choose one path rather than another.
        endNode = tile_map[10][10];                                                      // Arrival is at the base.

        if (startNode == endNode) {
            //System.out.println("Retour à la base");                                    // TODO: Add to log.
        }
    }

    public final Point Resolve(Tile[][] tile_map) {
        Erase(tile_map);
        Run(tile_map);
        int maxdistance = (int) EndNode().distanceFromStart;                             // Calculate max distance.
        return BuildPath(maxdistance);
    }

    public void Erase(Tile[][] tile_map) {

        listNodes = null;                                                                // Erase lists.
        listEdges = null;

        for (Tile[] tiles : tile_map) {                                                  // Erase distances and precursors.
            for (int column = 0; column < tile_map.length; column++) {
                tiles[column].distanceFromStart = Double.POSITIVE_INFINITY;
                tiles[column].precursor = null;
            }
        }
        startNode.distanceFromStart = startNode.getCost();                               // Initial node.
    }

    protected void Run(Tile[][] tile_map) {
        DistanceEstimated(tile_map);
        ArrayList<Node> listNodes = ListNodes(tile_map);

        boolean solution = false;

        while (listNodes.size() != 0 && !solution) {

            Node currentNode = listNodes.get(0);                                         // Search for node with shortest distance.
            for (Node snode : listNodes) {

                if (snode.distanceFromStart + snode.distanceEstimated < currentNode.distanceFromStart + currentNode.distanceEstimated) {
                    currentNode = snode;
                }
            }

            if (currentNode.equals(EndNode())) {
                solution = true;
            } else {
                ArrayList<Edge> outboundEdges = ListOutboundEdges(currentNode, tile_map);

                for (Edge arc : outboundEdges) {
                    if (arc.source.distanceFromStart + arc.cost < arc.destination.distanceFromStart) {

                        arc.destination.distanceFromStart = arc.source.distanceFromStart + arc.cost;
                        arc.destination.precursor = arc.source;
                    }
                }
                listNodes.remove(currentNode);
            }
        }
    }

    public Point BuildPath(int maxdistance) {                                                                  

        Point[] points = new Point[maxdistance];
        Tile currentNode = endNode;
        Tile previousNode = (Tile) endNode.precursor;

        int cpt = 0;

        while (previousNode != null) {
            cpt++;
            points[cpt] = currentNode.toPoint();
            currentNode = previousNode;
            previousNode = (Tile) currentNode.precursor;
        }
        return points[cpt];
    }

    public void DistanceEstimated(Tile[][] tile_map) {

        for (int line = 0; line < tile_map.length; line++) {
            for (int column = 0; column < tile_map.length; column++) {
                /* Heuristic */
                tile_map[line][column].distanceEstimated = Math.abs(startNode.getPosition().x - line) + Math.abs(startNode.getPosition().y - column);
            }
        }
    }

    public Node EndNode() {
        return endNode;
    }

    public ArrayList<Node> ListNodes(Tile[][] tile_map) {

        if (listNodes == null) {
            listNodes = new ArrayList();
            for (Tile[] tiles : tile_map) {
                listNodes.addAll(Arrays.asList(tiles));
            }
        }
        return listNodes;
    }

    public ArrayList<Edge> ListOutboundEdges(Node source, Tile[][] tile_map) {

        ArrayList<Edge> listOutboundEdges = new ArrayList();
        int line = ((Tile) source).getPosition().x;
        int column = ((Tile) source).getPosition().y;

        if (tile_map[line][column].Accessible()) {

            /* UP */
            if (line - 1 >= 0 && tile_map[line - 1][column].Accessible()) {
                listOutboundEdges.add(new Edge(tile_map[line][column], tile_map[line - 1][column], tile_map[line - 1][column].getCost()));
            }

            /* DOWN */
            if (line + 1 < tile_map.length && tile_map[line + 1][column].Accessible()) {
                listOutboundEdges.add(new Edge(tile_map[line][column], tile_map[line + 1][column], tile_map[line + 1][column].getCost()));
            }

            /* RIGHT */
            if (column - 1 >= 0 && tile_map[line][column - 1].Accessible()) {
                listOutboundEdges.add(new Edge(tile_map[line][column], tile_map[line][column - 1], tile_map[line][column - 1].getCost()));
            }

            /* LEFT */
            if (column + 1 < tile_map.length && tile_map[line][column + 1].Accessible()) {
                listOutboundEdges.add(new Edge(tile_map[line][column], tile_map[line][column + 1], tile_map[line][column + 1].getCost()));
            }

        }

        return listOutboundEdges;

    }

    public ArrayList<Edge> ListEdges(Tile[][] tile_map) {

        if (listEdges == null) {
            listEdges = new ArrayList();
            for (Tile[] tiles : tile_map) {                                                             // Browse nodes.
                for (int column = 0 ; column < tile_map.length ; column++) {
                    ArrayList<Edge> Edges = ListOutboundEdges(tiles[column], tile_map);
                    listEdges.addAll(Edges);
                }
            }
        }
        return listEdges;
    }
}

// TODO : Change fct accessibility


