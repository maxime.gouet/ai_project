/* author: Lucas Guilbert */
package core;

import core.deplacement.*;
import core.planet.Planet;
import core.planet.map.tiles.Tile;
import core.robots.*;
import core.robots.Robot;

import java.awt.*;
import java.util.ArrayList;

public class Simulation {
    private final Planet planet;
    private final Centralisateur centralisateur;
    private final ArrayList<Robot> robots;
    private int rounds;

    Simulation() {
        planet = new Planet();
        robots = new ArrayList<>();

        centralisateur = new Centralisateur(new Point(10, 10));
        centralisateur.setStrategie(new Still());

        Point STARTING_POINT = new Point(10, 10);
        robots.add(new Extracteur(STARTING_POINT));
        robots.add(new Extracteur(STARTING_POINT));
        robots.add(new Extracteur(STARTING_POINT));
        robots.add(new Constructeur(STARTING_POINT));
        robots.add(new Agriculteur(STARTING_POINT));
        robots.add(new Constructeur(STARTING_POINT));
        robots.add(new Constructeur(STARTING_POINT));
        robots.add(new Recolteur(STARTING_POINT));
        robots.add(new Recolteur(STARTING_POINT));
        robots.add(new Recolteur(STARTING_POINT));
        robots.add(new Agriculteur(STARTING_POINT));
        robots.add(new Agriculteur(STARTING_POINT));
        robots.add(centralisateur);

        for (Robot robot : robots) {
            robot.setKnowledge(blankMap()); // Pour donner une connaissance vierge de la map aux robots
            if (robot instanceof Extracteur) {
                robot.setStrategie(new Q_Learning());
            }
            if (!(robot instanceof Centralisateur)) {
                centralisateur.ajoute(robot);
                robot.ajoute(centralisateur);
            }
        }
        rounds = 0;
    }

    public void run() {
        /* Planet logic */
        planet.checkForMetamorphose();
        planet.extractWater();
        planet.checkForPipelineToBeFullyConnected();
        planet.checkExploited();

        /* Robot logic */
        for (Robot robot : robots) {
            robot.mooving();
            robot.work(planet.getMap().getTile(robot.getCurrent_position()));
            robot.capture(planet);
        }

        rounds++;
    }
    /** Cette method a était réaliser par Olivier Lallinec */
    private Tile[][] blankMap() {
        Tile[][] blankMap = new Tile[21][21];
        for (int row = 0; row < 21; row++) {
            for (int col = 0; col < 21; col++) {
                blankMap[row][col] = new TUnknown(row, col, 'U');
            }
        }
        return blankMap;
    }

    public int getRounds() {
        return rounds;
    }

    public Planet getPlanet() {
        return planet;
    }

    public ArrayList<Robot> getRobots() {
        return robots;
    }
}
