/* author: Lucas Guilbert */
package core;

import processing.core.PApplet;

import java.text.DecimalFormat;

public class Utils {
    public static float randomBetweenBoundaries(float min, float max) {
        PApplet sketch = MainSketch.getInstance();

        return sketch.random(min, max+1);
    }

    public static int randomIntBetweenBoundaries(int min, int max) {
        PApplet sketch = MainSketch.getInstance();

        return (int)sketch.random(min, max+1);
    }

    public static float random0_1() {
        return (float)Math.random();
    }

    public static String rateFormatting(float rate) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        return df.format(rate);
    }
}
