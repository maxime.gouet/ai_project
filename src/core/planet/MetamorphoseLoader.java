/* author: Lucas Guilbert */
package core.planet;

import java.io.FileNotFoundException;

public interface MetamorphoseLoader {
    MetamorphoseDistribution load(String filePath) throws FileNotFoundException;
}
