/* author: Lucas Guilbert */
package core.planet.map;

import core.planet.MetamorphoseDistribution;
import core.planet.MetamorphoseLoader;
import core.planet.map.tiles.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MetamorphoseLoaderFromFile implements MetamorphoseLoader {
    @Override
    public MetamorphoseDistribution load(String filePath) throws FileNotFoundException {
        MetamorphoseDistribution metamorphoseDistribution = new MetamorphoseDistribution();

        File map = new File(filePath);

        if (!map.exists()) return null;

        Scanner scanner = new Scanner(map);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] array = line.split(" ");
            TType initial = charToTType(array[0].charAt(0));
            TType meta = charToTType(array[1].charAt(0));
            float rate = Float.parseFloat(array[2]);
            metamorphoseDistribution.addMetamorphoseElement(initial, meta, rate);
        }
        scanner.close();

        return metamorphoseDistribution;
    }

    private TType charToTType(char c) {
        switch (c) {
            case 'Z' -> {
                return TType.Obstacle;
            }
            case 'N' -> {
                return TType.Food;
            }
            case 'D' -> {
                return TType.Desert;
            }
            case 'L' -> {
                return TType.Lake;
            }
            case 'S' -> {
                return TType.DriedMeadow;
            }
            case 'H' -> {
                return TType.NormalMeadow;
            }
            case 'G' -> {
                return TType.FattishMeadow;
            }
            case 'F' -> {
                return TType.Forest;
            }
            case 'M' -> {
                return TType.Ore;
            }
            case 'P' -> {
                return TType.Stone;
            }
            default -> {
                return null;
            }
        }
    }
}
