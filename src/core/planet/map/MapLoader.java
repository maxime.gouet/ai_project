/* author: Lucas Guilbert */
package core.planet.map;

import core.planet.map.tiles.Tile;

import java.io.FileNotFoundException;

public interface MapLoader {
  Tile[][] load(String filePath) throws FileNotFoundException;
}
