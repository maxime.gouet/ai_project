/* author: Lucas Guilbert */
package core.planet.map;

import core.Utils;
import core.planet.map.tiles.Tile;

import java.awt.*;
import java.io.FileNotFoundException;

public class Map {
    private final MapLoader loader;
    private final ExoLoader exoLoader;
    private int tilesPartOfExo;

    public Tile[][] tiles;

    public Map() {
        loader = new MapLoaderFromFile();
        exoLoader = new ExoLoaderFromFile();
        tilesPartOfExo = 0;
    }

    public void load(String filePath, String exoFilePath) throws FileNotFoundException {
        tiles = loader.load(filePath);
        tilesPartOfExo = 21 * 21 - exoLoader.load(tiles, exoFilePath);
    }

    public Tile getTile(Point pos) {
        return getTile(pos.x, pos.y);
    }

    public Tile getTile(int x, int y) {
        if (x > 20 || y > 20 || x < 0 || y < 0) return null;
        return tiles[x][y];
    }

    public void replace(Tile newTile) {
        tiles[newTile.getPosition().x][newTile.getPosition().y] = newTile;
    }

    public Tile getRandomTileNotPartOfExo() {
        int x, y;

        do {
            x = Utils.randomIntBetweenBoundaries(0, 20);
            y = Utils.randomIntBetweenBoundaries(0, 20);
        } while(tiles[x][y].isPartOfExo());

        return tiles[x][y];
    }

    public int getTilesPartOfExo() { return tilesPartOfExo; }

    public Tile[][] getTiles() {
        return tiles;
    }
}
