/* author: Lucas Guilbert */
package core.planet.map;

import core.planet.map.tiles.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MapLoaderFromFile implements MapLoader {
  @Override
  public Tile[][] load(String filePath) throws FileNotFoundException {
    char[][] tiles;

    File map = new File(filePath);

    if (!map.exists()) return null;

    Scanner scanner = new Scanner(map);

    String firstLine = scanner.nextLine();
    if (firstLine.equals("")) return null;

    String[] arr = firstLine.split(";");
    int w = Integer.parseInt(arr[0]);
    int h = Integer.parseInt(arr[1]);
    tiles = new char[w][h];

    int row = 0;
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      int col = 0;
      for (char c : line.toCharArray()) {
        tiles[col][row] = c;
        col++;
      }
      row++;
    }
    scanner.close();

    return parseCharMap(tiles, w, h);
  }

  private Tile[][] parseCharMap(char[][] tiles, int w, int h) {
    Tile[][] map = new Tile[w][h];

    for (int row = 0; row < h; row++) {
      for (int col = 0; col < w; col++) {
        switch (tiles[row][col]) {
          case 'B' -> map[row][col] = new TBase(row, col, 'B');
          case 'Z' -> map[row][col] = new TObstacle(row, col, 'Z');
          case 'N' -> map[row][col] = new TFood(row, col, 'N');
          case 'D' -> map[row][col] = new TDesert(row, col, 'D');
          case 'L' -> map[row][col] = new TLake(row, col, 'L');
          case 'S' -> map[row][col] = new TDriedMeadow(row, col, 'S');
          case 'H' -> map[row][col] = new TNormalMeadow(row, col, 'H');
          case 'G' -> map[row][col] = new TFattishMeadow(row, col, 'G');
          case 'F' -> map[row][col] = new TForest(row, col, 'F');
          case 'M' -> map[row][col] = new TOre(row, col, 'M');
          case 'P' -> map[row][col] = new TStone(row, col, 'P');
        }
      }
    }

    return map;
  }
}
