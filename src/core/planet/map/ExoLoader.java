/* author: Lucas Guilbert */
package core.planet.map;

import core.planet.map.tiles.Tile;

import java.io.FileNotFoundException;

public interface ExoLoader {
  int load(Tile[][] tiles, String filePath) throws FileNotFoundException;
}
