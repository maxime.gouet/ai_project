/* author: Lucas Guilbert */
package core.planet.map.tiles;

import java.awt.*;

public class TLake extends Tile {
  private float waterStock;
  private boolean isSource;
  private boolean isFullyConnected;

  public TLake(int x, int y, char name) {
    super(new Point(x, y), name);
    waterStock = 200000;
    TType_ = TType.Lake;
    isSource = false;
    isFullyConnected = false;
  }

  public void extractWatter(float value) {
    if (waterStock <= 0) {
      waterStock = 0;
      return;
    }

    waterStock -= value;
  }

  public boolean isFullyConnected() {
    return isFullyConnected;
  }

  public void setFullyConnected(boolean val) {
    isFullyConnected = val;
  }

  public void setAsSource() {
    isSource = true;
  }

  public boolean isSource() { return isSource; }

  // Return the value that has been extracted since the beginning
  public float getWaterExtracted() {
    return 200000 - waterStock;
  }

  @Override
  public String toString() {
    return "[TLake]: " + super.toString();
  }

  public TType getTType(){
    return TType_;
  }

  public Tile clone() {
    return new TLake(this.position.x,this.position.y,this.name);
  }
}
