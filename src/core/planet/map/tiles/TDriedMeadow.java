/* author: Lucas Guilbert */
package core.planet.map.tiles;

import java.awt.*;

public class TDriedMeadow extends Tile {
    private boolean into_food;
    public TDriedMeadow(int x, int y, char name) {
        super(new Point(x, y), name);
        TType_ = TType.DriedMeadow;
        into_food = false;
    }

    @Override
    public String toString() {
        return "[TDriedMeadow]: " + super.toString();
    }

    public TType getTType() {
        return TType_;
    }

    public Tile clone() {
        return new TDriedMeadow(this.position.x,this.position.y,this.name);
    }

    public boolean getInto_Food(){
        return into_food;
    }
    public void setInto_Food(boolean into_food){
        this.into_food = into_food;
    }

    public boolean isInto_food() {
        return into_food;
    }
}
