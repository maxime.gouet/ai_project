/* author: Lucas Guilbert */
package core.planet.map.tiles;

import core.graph.Node;

import java.awt.*;

public abstract class Tile extends Node {
    protected Point position;
    protected final char name;
    protected boolean isPartOfExo;
    protected TType TType_;
    protected boolean hasAPipeline;

    protected Tile(Point pos, char _name) {
        position = pos;
        name = _name;
        isPartOfExo = false;
        hasAPipeline = false;
    }

    public void setIfPartOfExo(boolean val) {
        isPartOfExo = val;
    }

    public boolean isPartOfExo() {
        return isPartOfExo;
    }

    public Point getPosition() {
        return position;
    }

    public char getName() {
        return name;
    }

    public void constructPipeline() { hasAPipeline = true; }

    public boolean HasAPipeline() { return hasAPipeline; }

    public void setHasAPipeline(boolean HasAPipeline) { this.hasAPipeline = HasAPipeline; }

    @Override
    public String toString() {
        String str = "Position(" + position.x + ":" + position.y + ")";
        if (isPartOfExo) {
            return str + "\n" +
                    "Part of Exo";
        }
        return str;
    }

    public Point toPoint() {
        Point cpoint = new Point(position.x, position.y);
        return cpoint;
    }

    public TType getTType() {
        return TType_;
    }

    public abstract Tile clone();

    public boolean Accessible() {
        return (getTType().equals(TType.NormalMeadow) || getTType().equals(TType.DriedMeadow) || getTType().equals(TType.FattishMeadow) || getTType().equals(TType.Base) || getTType().equals(TType.Desert) || getTType().equals(TType.Lake) || getTType().equals(TType.Stone) || getTType().equals(TType.Unknown));
    }

    public double getCost() {    // Choose one path rather than another
        return switch (TType_) {
            case Base, Food, Desert, Stone, NormalMeadow, FattishMeadow, DriedMeadow, Unknown -> 1;
            case Lake -> 3;
            // Moving on the water is slower
            case Ore, Forest -> 2;
            default -> Double.POSITIVE_INFINITY;
        };
    }
}

