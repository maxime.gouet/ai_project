/* author: Lucas Guilbert */
package core.planet.map.tiles;

public enum TType {Base,Desert,DriedMeadow, FattishMeadow,Food,Forest,Lake,NormalMeadow,Obstacle,Ore,Stone,Unknown}
