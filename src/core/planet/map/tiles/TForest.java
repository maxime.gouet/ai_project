/* author: Lucas Guilbert */
package core.planet.map.tiles;

import java.awt.*;

public class TForest extends Tile {
  public TForest(int x, int y, char name) {
    super(new Point(x, y), name);
    TType_ = TType.Forest ;
  }

  @Override
  public String toString() {
    return "[TForest]: " + super.toString();
  }

  public TType getTType(){
    return TType_;
  }

  public Tile clone() {
    return new TForest(this.position.x,this.position.y,this.name);
  }
}
