/* author: Lucas Guilbert */
package core.planet.map.tiles;

import java.awt.*;

public class TOre extends Tile {
  private int oreStock;
  private boolean isExploited;

  public TOre(int x, int y, char name) {
    super(new Point(x, y), name);
    TType_ = TType.Ore ;
    oreStock = 100;
    isExploited = false;
  }

  @Override
  public String toString() {
    return "[TOre]: " + super.toString() + "\nExploited: " + isExploited + "\nstock: " + oreStock;
  }

  // Returns the value that has been extracted since the beginning
  public float getOreMined() {
    return 100 - oreStock;
  }

  public boolean isExploited() {
    return isExploited;
  }

  /** Cette method a était réaliser par Olivier Lallinec */
  public void mineOre(int oreMined) {
    oreStock -= oreMined;
    if (oreStock <= 0) {
      oreStock = 0;
    }

  }

  public TType getTType(){
    return TType_;
  }

  public void setisExploited(boolean isExploited){
    this.isExploited = isExploited;
  }

  public Tile clone() {
    return new TOre(this.position.x,this.position.y,this.name);
  }

  public int getOreStock() {
    return oreStock;
  }
}
