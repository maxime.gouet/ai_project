/* author: Lucas Guilbert */
package core.planet.map.tiles;

import java.awt.*;

public class TDesert extends Tile{
  public TDesert(int x, int y, char name) {
    super(new Point(x, y), name);
    TType_ = TType.Desert ;
  }

  @Override
  public String toString() {
    return "[TDesert]: " + super.toString();
  }

  public TType getTType(){
    return TType_;
  }

  public Tile clone() {
    return new TDesert(this.position.x,this.position.y,this.name);
  }
}
