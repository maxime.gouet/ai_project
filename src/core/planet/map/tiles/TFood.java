/* author: Lucas Guilbert */
package core.planet.map.tiles;

import java.awt.*;

public class TFood extends Tile {

  private boolean isExploited;
  private int foodStock;

  public TFood(int x, int y, char name) {
    super(new Point(x, y), name);
    TType_ = TType.Food ;
    foodStock = 100;
    isExploited = false;
  }

  @Override
  public String toString() {
    return "[TFood]: " + super.toString();
  }

  public TType getTType(){
    return TType_;
  }

  public boolean isExploited() {
    return isExploited;
  }

  /** Cette method a était réaliser par Olivier Lallinec */
  public void recoltFood(int foodRecolted){
    foodStock -= foodRecolted;
    if (foodStock <= 0){
      foodStock = 0;
    }
  }

  public int getFoodStock(){
    return foodStock;
  }
  public void setFoodStock(int foodStock){
    this.foodStock = foodStock;
  }
  public void setExploited(boolean isExploited){
    this.isExploited = isExploited;
  }

  public Tile clone() {
    return new TFood(this.position.x,this.position.y,this.name);
  }
}
