/* author: Lucas Guilbert */
package core.planet.map.tiles;

import java.awt.*;

public class TObstacle extends Tile {

  public TObstacle(int x, int y, char name) {
    super(new Point(x, y), name);
    TType_ = TType.Obstacle ;
  }

  @Override
  public String toString() {
    return "[TObstacle]: " + super.toString();
  }

  public TType getTType(){
    return TType_;
  }

  public Tile clone() {
    return new TObstacle(this.position.x,this.position.y,this.name);
  }
}
