/* author: Lucas Guilbert */
package core.planet.map.tiles;

import java.awt.*;

public class TBase extends Tile {
    private int foodStock;
    private int oreStock;
    private int waterStock;

    public int getFoodStock() {
        return foodStock;
    }

    public void setFoodStock(int foodStock) {
        this.foodStock = foodStock;
    }

    public int getWaterStock() {
        return waterStock;
    }

    public void addWater(int waterStock) {
        this.waterStock += waterStock;
    }

    public void takeWater(int water) { this.waterStock -= water; }

    public int getOreStock() {
        return oreStock;
    }

    public void addOre(int oreStock) {
        this.oreStock += oreStock;
    }

    public TBase(int x, int y, char name) {
        super(new Point(x, y), name);
        foodStock = 0;
        oreStock = 0;
        waterStock = 0;
        TType_ = TType.Base;
    }

    @Override
    public String toString() {
        return "[TBase]: " + super.toString();
    }

    public TType getTType() {
        return TType_;
    }

    @Override
    public Tile clone() {
        return new TBase(this.position.x, this.position.y, this.name);
    }
}
