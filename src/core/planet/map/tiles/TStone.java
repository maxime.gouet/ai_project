/* author: Lucas Guilbert */
package core.planet.map.tiles;

import java.awt.*;

public class TStone extends Tile {

  public TStone(int x, int y, char name) {
    super(new Point(x, y), name);
    TType_ = TType.Stone ;
  }

  @Override
  public String toString() {
    return "[TStone]: " + super.toString();
  }

  public TType getTType(){
    return TType_;
  }

  public Tile clone() {
    return new TStone(this.position.x,this.position.y,this.name);
  }
}
