/* author: Lucas Guilbert */
package core.planet.map;

import core.planet.map.tiles.Tile;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ExoLoaderFromFile implements ExoLoader {

  @Override
  public int load(Tile[][] tiles, String filePath) throws FileNotFoundException {
    int count = 0;
    File map = new File(filePath);

    if (!map.exists()) return -1;

    Scanner scanner = new Scanner(map);

    int row = 0;
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      int col = 0;
      for (char c : line.toCharArray()) {
        if (c == '1') {
          tiles[col][row].setIfPartOfExo(true);
          count++;
        }
        col++;
      }
      row++;
    }
    scanner.close();

    return count;
  }
}
