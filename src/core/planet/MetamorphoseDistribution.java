/* author: Lucas Guilbert */
package core.planet;

import core.planet.map.tiles.TType;

import java.util.HashMap;
import java.util.Map;

public class MetamorphoseDistribution {
    // Class found here: https://stackoverflow.com/questions/20327958/random-number-with-probabilities
    private static class DistributedRNG {
        private final Map<TType, Double> distribution;
        private double distSum;

        public DistributedRNG() {
            distribution = new HashMap<>();
        }

        public void add(TType value, double distribution) {
            if (this.distribution.get(value) != null) {
                distSum -= this.distribution.get(value);
            }
            this.distribution.put(value, distribution);
            distSum += distribution;
        }

        public TType getDistributedRandomTType() {
            double rand = Math.random();
            double ratio = 1.0f / distSum;
            double tempDist = 0;
            for (TType i : distribution.keySet()) {
                tempDist += distribution.get(i);
                if (rand / ratio <= tempDist) {
                    return i;
                }
            }

            return null;
        }
    }

    private final Map<TType, DistributedRNG> map;

    public MetamorphoseDistribution() {
        map = new HashMap<>();
    }

    public void addMetamorphoseElement(TType initial, TType meta, float rate) {
        if (!map.containsKey(initial)) {
            DistributedRNG distribution = new DistributedRNG();
            distribution.add(meta, rate);
            map.put(initial, distribution);
        } else {
            map.get(initial).add(meta, rate);
        }
    }

    public TType getMetamorphoseType(TType initial) {
        if (map.containsKey(initial)) {
            return map.get(initial).getDistributedRandomTType();
        }
        return null;
    }
}