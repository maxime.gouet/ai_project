/* author: Lucas Guilbert */
package core.planet;

import core.deplacement.A_star;
import core.planet.map.Map;
import core.planet.map.MetamorphoseLoaderFromFile;
import core.planet.map.tiles.*;
import core.robots.Type_robot;
import core.robots.WorkChoice;
import net.sourceforge.jFuzzyLogic.FIS;

import java.awt.*;
import java.io.FileNotFoundException;
import java.util.LinkedList;

public class Planet {
    private final Map map;
    private final FIS fis;
    private float metamorphoseRate;
    private MetamorphoseDistribution metamorphoseDistribution;

    public Planet() {
        metamorphoseRate = 0;
        map = new Map();
        metamorphoseDistribution = null;
        try {
            map.load("resources/map.txt", "resources/exo.txt");
            metamorphoseDistribution = new MetamorphoseLoaderFromFile().load("resources/metamorphose.txt");
        } catch (FileNotFoundException e) {
            System.err.println("Error while loading map.txt or exo.txt or metamorphose.txt : " + e.getMessage());
            System.exit(-1);
        }

        fis = FIS.load("resources/fcl/Metamorphose.fcl", true);
        if (fis == null) {
            System.err.println("FIS NULL");
            System.exit(-1);
        }
    }

    public void checkForPipelineToBeFullyConnected() {
        for (int x = 0; x < 21; x++) {
            for (int y = 0; y < 21; y++) {
                Tile tile = map.getTile(x, y);
                if (tile instanceof TLake && (((TLake) tile).isSource())) {
                    A_star test = new A_star();
                    test.mooving(map.getTiles(), null, tile.getPosition(), null, null);
                    LinkedList<A_star.Node> path = test.getPath();
                    path.removeFirst();

                    boolean connected = true;
                    while (!path.isEmpty()) {
                        A_star.Node node = path.removeFirst();
                        Tile current = map.getTile(node.x, node.y);
                        if (!current.HasAPipeline() && current.getTType() != TType.Base) connected = false;
                    }
                    ((TLake)tile).setFullyConnected(connected);
                }
            }
        }
    }

    public void checkForMetamorphose() {
        if (percentageOfWaterExtracted() != 0 && percentageOfOreMined() != 0) {
            fis.setVariable("ore", percentageOfOreMined());
            fis.setVariable("water", percentageOfWaterExtracted());
            fis.evaluate();

            metamorphoseRate = (float) fis.getVariable("metamorphose").getValue();
            processMetamorphose();
        }
    }

    public void extractWater() {
        Tile base = map.getTile(10, 10);
        for (int x = 0; x < 21; x++) {
            for (int y = 0; y < 21; y++) {
                Tile tile = map.getTile(x, y);
                if (tile instanceof TLake && ((TLake) tile).isSource() && ((TLake)tile).isFullyConnected()) {
                    ((TLake)tile).extractWatter(1000);
                    ((TBase)base).addWater(1000);
                }
            }
        }
    }

    private void processMetamorphose() {
        int nbTilesNotPartOfExo = 21 * 21 - map.getTilesPartOfExo();
        int nbTilesToMetamorphose = (int) Math.floor(nbTilesNotPartOfExo * (metamorphoseRate/100));

        for (int i = 0; i < nbTilesToMetamorphose; i++) {
            Tile tile = map.getRandomTileNotPartOfExo();
            TType metaType = metamorphoseDistribution.getMetamorphoseType(tile.getTType());
            if (metaType != null) {
                Tile newTile = createNewTileFromTType(metaType, tile.getPosition());
                map.replace(newTile);
            }
        }
    }

    private Tile createNewTileFromTType(TType type, Point pos) {
        switch (type) {
            case Base -> {
                return new TBase(pos.x, pos.y, 'B');
            }
            case Desert -> {
                return new TDesert(pos.x, pos.y, 'D');
            }
            case DriedMeadow -> {
                return new TDriedMeadow(pos.x, pos.y, 'S');
            }
            case FattishMeadow -> {
                return new TFattishMeadow(pos.x, pos.y, 'G');
            }
            case Food -> {
                return new TFood(pos.x, pos.y, 'N');
            }
            case Forest -> {
                return new TForest(pos.x, pos.y, 'F');
            }
            case Lake -> {
                return new TLake(pos.x, pos.y, 'L');
            }
            case NormalMeadow -> {
                return new TNormalMeadow(pos.x, pos.y, 'H');
            }
            case Obstacle -> {
                return new TObstacle(pos.x, pos.y, 'Z');
            }
            case Ore -> {
                return new TOre(pos.x, pos.y, 'M');
            }
            default -> {
                return new TStone(pos.x, pos.y, 'P');
            }
        }
    }

    /* ACCESSEURS */

    public double getMetamorphoseRate() {
        return metamorphoseRate;
    }

    public float percentageOfOreMined() {
        float mined = 0;
        float total = 0;

        for (int x = 0; x < 21; x++) {
            for (int y = 0; y < 21; y++) {
                Tile tile = map.getTile(x, y);
                if (tile instanceof TOre) {
                    mined += ((TOre) tile).getOreMined();
                    total += 100;
                }
            }
        }

        return mined * 100 / total;
    }

    public float percentageOfWaterExtracted() {
        float extracted = 0;
        float total = 0;

        for (int x = 0; x < 21; x++) {
            for (int y = 0; y < 21; y++) {
                Tile tile = map.getTile(x, y);
                if (tile instanceof TLake) {
                    extracted += ((TLake) tile).getWaterExtracted();
                    total += 100000;
                }
            }
        }

        return extracted * 100 / total;
    }

    public Map getMap() {
        return map;
    }

    public void checkExploited(){
        TOre ore;
        TFood food;
        TDriedMeadow driedMeadow;
        TNormalMeadow normalMeadow;
        TFattishMeadow fattishMeadow;
        for (int x = 0; x < 21; x++) {
            for (int y = 0; y < 21; y++) {
                Tile tile = map.getTile(x, y);
                if (tile.getTType().equals(TType.Ore)){
                    ore = (TOre) tile;
                    if (ore.getOreStock() == 0){
                        if(map.getTile(x, y).HasAPipeline()){
                            map.replace(new TStone(x,y,'P'));
                            map.getTile(x, y).setHasAPipeline(true);
                        }else{
                            map.replace(new TStone(x,y,'P'));
                        }
                    }

                } else if (tile.getTType().equals(TType.Food)){
                    food = (TFood) tile;
                    if (food.getFoodStock() == 0){
                        map.replace(new TDriedMeadow(x,y,'S'));
                    }
                } else if (tile.getTType().equals(TType.DriedMeadow)){
                    driedMeadow = (TDriedMeadow) tile;
                    if (driedMeadow.isInto_food()){
                        if(map.getTile(x, y).HasAPipeline()){
                            map.replace(new TFood(x,y,'N'));
                            map.getTile(x, y).setHasAPipeline(true);
                        }else{
                            map.replace(new TFood(x,y,'N'));
                        }
                    }
                } else if (tile.getTType().equals(TType.NormalMeadow)){
                    normalMeadow = (TNormalMeadow) tile;
                    if (normalMeadow.isInto_food()){
                        if(map.getTile(x, y).HasAPipeline()){
                            map.replace(new TFood(x,y,'N'));
                            map.getTile(x, y).setHasAPipeline(true);
                        }else{
                            map.replace(new TFood(x,y,'N'));
                        }
                    }
                }else if (tile.getTType().equals(TType.FattishMeadow)){
                    fattishMeadow = (TFattishMeadow) tile;
                    if (fattishMeadow.isInto_food()){
                        if(map.getTile(x, y).HasAPipeline()){
                            map.replace(new TFood(x,y,'N'));
                            map.getTile(x, y).setHasAPipeline(true);
                        }else{
                            map.replace(new TFood(x,y,'N'));
                        }
                    }
                }
            }
        }
    }
}
