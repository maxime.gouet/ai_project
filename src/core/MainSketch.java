/* author: Lucas Guilbert */
package core;

import core.view.InformationFrame;
import core.view.MapFrame;
import core.view.RobotView;
import processing.core.PApplet;

public class MainSketch extends PApplet {
  private static MainSketch instance;

  private final static int TILE_SIZE = 32;

  private Simulation simulation;

  private InformationFrame infoFrame;
  private MapFrame mapFrame;
  private RobotView robotView;

  public void settings() {
    simulation = new Simulation();
    mapFrame = new MapFrame(simulation.getPlanet().getMap().getTiles());
    robotView = new RobotView(simulation.getRobots());

    size(mapFrame.frameWidth() + 300, mapFrame.frameHeight());

    infoFrame = new InformationFrame(21 * TILE_SIZE, simulation);
  }

  public void draw() {
    frameRate(2);
    simulation.run();
    background(255, 255, 255);

    mapFrame.display(instance);
    infoFrame.display(instance);
    robotView.display(instance);
  }

  public static void main(String[] args) {
    String[] processingArgs = {"IA Project - God view"};
    instance = new MainSketch();
    instance.runSketch(processingArgs);
  }

  public static MainSketch getInstance() {
    return instance;
  }

  public static int getTileSize() {
    return TILE_SIZE;
  }
}
