package core.robots;

import core.deplacement.A_star;
import core.planet.map.tiles.*;
import core.deplacement.Q_Learning;

import java.awt.*;

public class Recolteur extends Robot {
    int foodStock;
    private WorkChoice currentTask;
    private boolean isExploiting;

    public Recolteur(Point pos) {
        super(pos);
        foodStock = 0;
        type = Type_robot.Recolteur;
        currentTask = workStrategy.whatDoIdo();
        isExploiting = currentTask == WorkChoice.EXPLOITATION;
        setStrategie(new Q_Learning());
    }

    public int getFoodStock() {
        return foodStock;
    }

    public void setFoodStock(int food) {
        this.foodStock = food;
    }

    @Override
    /** Cette method a était réaliser par Olivier Lallinec */
    public void work(Tile tile) {
        TFood food;
        TBase base;
        if (tile.getTType() == TType.Food && foodStock==0) {
            food = (TFood) tile;
            if(food.getFoodStock()>10) {
                food.setFoodStock(food.getFoodStock() - 10);
                foodStock = 10;
            } else {
                foodStock = food.getFoodStock();
                food.setFoodStock(0);
            }
            food = (TFood) knowledge[current_position.x][current_position.y];
            food.setExploited(true);
            this.setStrategie(new A_star());
        } else if(tile.getTType() == TType.Base && foodStock!=0){
            base = (TBase) tile;
            base.setFoodStock(base.getFoodStock() + foodStock);
            this.setStrategie(new Q_Learning());
        }
    }

    /** Cette method a était en partie réaliser par Maxime Gouet */
    @Override
    public void mooving() {
        Point posToMoveTo = strategie.mooving(knowledge,exploration_value, current_position, this.getType(), workStrategy.whatDoIdo());
        if (!(knowledge[current_position.x][current_position.y] instanceof TLake) &&
                !(knowledge[current_position.x][current_position.y] instanceof TObstacle)) {
            current_position = posToMoveTo;
        }
    }
    /** Cette method a était réaliser par Maxime Gouet */
    public void actualise(Tile[][] tiles,int[][] exploration_value) {
        this.knowledge = tiles;
        this.exploration_value = exploration_value;
    }

    public Recolteur() {
        super(null);
        type = Type_robot.Recolteur;
    }
}
