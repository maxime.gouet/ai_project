/* author: Lucas Guilbert */
package core.robots;

import core.Utils;

public class WorkStrategy {
    private final float EXPLORATION_PROBABILITY;
    private final float EXPLOITATION_PROBABILITY;
    private final EGreedy algo;

    public WorkStrategy() {
        EXPLORATION_PROBABILITY = 0.95f;
        EXPLOITATION_PROBABILITY = 0.05f;
        float EPSILON = 0.1f;
        algo = new EGreedy(EPSILON, 2);
    }

    // Index 0 -> Exploration / Index 1 -> Exploitation
    public WorkChoice whatDoIdo() {
        int choice = algo.selectChoice();
        float reward;
        if (choice == 0) {
            reward = simulateReward(EXPLORATION_PROBABILITY);
            algo.update(choice, reward);
            return WorkChoice.EXPLORATION;
        } else {
            reward = simulateReward(EXPLOITATION_PROBABILITY);
            algo.update(choice, reward);
            return WorkChoice.EXPLOITATION;
        }
    }

    private float simulateReward(float probability) {
        if (Utils.random0_1() < probability) return 0.5f;
        return -0.5f;
    }

    private static class EGreedy {
        private final float epsilon;
        private final int[] counts;
        private final float[] values;

        EGreedy(float _epsilon, int nbChoices) {
            epsilon = _epsilon;
            counts = new int[nbChoices];
            values = new float[nbChoices];
        }

        int selectChoice() {
            if (sumFloatArray(values) == 0.0f || Utils.random0_1() < epsilon)
                return (int) (Utils.random0_1() * values.length);
            return indexOfMaxValue(values);
        }

        void update(int index, float reward) {
            counts[index] += 1;
            values[index] = ((counts[index] - 1) / (float) (counts[index])) * values[index] + (1 / (float) counts[index]) * reward;
        }

        private float sumFloatArray(float[] array) {
            float sum = 0.0f;
            for (float value : array) {
                sum += value;
            }
            return sum;
        }

        private int indexOfMaxValue(float[] array) {
            int index = 0;
            float max = -999999;
            for (int i = 0; i < array.length; i++) {
                if (array[i] > max) {
                    max = array[i];
                    index = i;
                }
            }

            return index;
        }
    }
}
