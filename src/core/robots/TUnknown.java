package core.robots;

import core.planet.map.tiles.TType;
import core.planet.map.tiles.Tile;

import java.awt.*;
/** Cette classe a était réaliser par Olivier Lallinec */
public class TUnknown extends Tile {
    public TUnknown(int x, int y, char _name) {
        super(new Point(x, y), _name);
        TType_ = TType.Unknown;
    }

    @Override
    public String toString() {
        return "[TUnknown]: " + super.toString();
    }

    public TType getTType(){
        return TType_;
    }

    @Override
    public Tile clone() {
        return null;
    }
}
