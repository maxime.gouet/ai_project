package core.robots;

import core.planet.map.tiles.Tile;

/** Cette classe a était fait par Maxime Gouet*/
public interface Observateur {
    public void actualise(Tile[][] tiles,int[][] exploration_value);
}
