package core.robots;

import core.deplacement.A_star;
import core.deplacement.Q_Learning;
import core.deplacement.Still;
import core.planet.map.tiles.*;

import java.awt.*;

public class Agriculteur extends Robot{
    private WorkChoice currentTask;
    private int processing_turn=0;
    private boolean isExploiting;

    /** Cette method a était en partie réaliser par Maxime Gouet */
    public Agriculteur(Point pos) {
        super(pos);
        currentTask = workStrategy.whatDoIdo();
        type = Type_robot.Agriculteur;
        isExploiting = currentTask == WorkChoice.EXPLOITATION;
        setStrategie(new Q_Learning());
    }

    /** Cette method a était réaliser par Maxime Gouet */
    @Override
    public void work(Tile tile) {
        if (!isExploiting) {
            currentTask = workStrategy.whatDoIdo();
            isExploiting = currentTask == WorkChoice.EXPLOITATION;
        }

        if (isExploiting) {
            TBase base = (TBase) knowledge[10][10];
            if (tile.getTType() == TType.DriedMeadow) {
                if (base.getWaterStock() < 400 * (60 - processing_turn)) {
                    currentTask = WorkChoice.EXPLORATION;
                } else {
                    if (processing_turn == 0) {
                        this.setStrategie(new Still());
                    }
                    processing_turn++;
                    base.takeWater(400);
                }
                if (processing_turn == 60) {
                    processing_turn = 0;
                    this.setStrategie(new Q_Learning());
                    ((TDriedMeadow) knowledge[current_position.x][current_position.y]).setInto_Food(true);
                    isExploiting = false;
                }
            } else if (tile.getTType() == TType.FattishMeadow) {
                if (base.getWaterStock() < 100 * (15 - processing_turn)) {
                    currentTask = WorkChoice.EXPLORATION;
                } else {
                    if (processing_turn == 0) {
                        this.setStrategie(new Still());
                    }
                    processing_turn++;
                    base.takeWater(100);
                }
                if (processing_turn == 15) {
                    processing_turn = 0;
                    this.setStrategie(new Q_Learning());
                    ((TFattishMeadow) knowledge[current_position.x][current_position.y]).setInto_Food(true);
                    isExploiting = false;
                }
            } else if (tile.getTType() == TType.NormalMeadow) {
                if (base.getWaterStock() < 200 * (30 - processing_turn)) {
                    currentTask = WorkChoice.EXPLORATION;
                } else {
                    if (processing_turn == 0) {
                        this.setStrategie(new Still());
                    }
                    processing_turn++;
                    base.takeWater(200);
                }
                if (processing_turn == 30) {
                    processing_turn = 0;
                    this.setStrategie(new Q_Learning());
                    ((TNormalMeadow) knowledge[current_position.x][current_position.y]).setInto_Food(true);
                    isExploiting = false;
                }
            } else {
                if (!(this.strategie instanceof Q_Learning)) {
                    this.setStrategie(new Q_Learning());
                    isExploiting = false;
                }
            }
        }
    }

    @Override
    /** Cette method a était en partie réaliser par Maxime Gouet */
    public void mooving() {
        Point posToMoveTo = strategie.mooving(knowledge, exploration_value, current_position, this.getType(), currentTask);
        /*if (!(knowledge[current_position.x][current_position.y] instanceof TLake) &&
            !(knowledge[current_position.x][current_position.y] instanceof TObstacle)) {
            current_position = posToMoveTo;
        }*/
        if (posToMoveTo != null) current_position = posToMoveTo;
    }
    /** Cette method a était réaliser par Maxime Gouet */
    public void actualise(Tile[][] tiles, int[][] exploration_value) {
        this.knowledge = tiles;
        this.exploration_value = exploration_value;
    }
}
