package core.robots;

import core.deplacement.A_star;
import core.deplacement.Q_Learning;
import core.planet.map.tiles.TLake;
import core.planet.map.tiles.TObstacle;
import core.planet.map.tiles.TType;
import core.planet.map.tiles.Tile;

import java.awt.*;

public class Constructeur extends Robot {
    private WorkChoice currentTask;
    private boolean isExploiting;
    private boolean returningToBase;

    public Constructeur(Point pos) {
        super(pos);
        type = Type_robot.Constructeur;
        currentTask = workStrategy.whatDoIdo();
        isExploiting = currentTask == WorkChoice.EXPLOITATION;
        returningToBase = false;
        setStrategie(new Q_Learning());
    }

    @Override
    public void work(Tile tile) {
        if (!isExploiting) {
            currentTask = workStrategy.whatDoIdo();
            isExploiting = currentTask == WorkChoice.EXPLOITATION;
        }

        if (isExploiting) {
            if (!returningToBase && tile.getTType() == TType.Lake) {
                ((TLake)tile).setAsSource();
                returningToBase = true;
                setStrategie(new A_star());
            } else if (tile.getTType() == TType.Base) {
                returningToBase = false;
                setStrategie(new Q_Learning());
                isExploiting = false;
            }

            if (returningToBase) {
                tile.constructPipeline();
            }
        }
    }

    @Override
    public void mooving() {
        Point posToMoveTo = strategie.mooving(knowledge,exploration_value ,current_position, this.getType(), currentTask);
        if (!(knowledge[current_position.x][current_position.y] instanceof TObstacle)) {
            current_position = posToMoveTo;
        }
    }
    /** Cette method a était réaliser par Maxime Gouet */
    public void actualise(Tile[][] tiles,int[][] exploration_value){
        this.knowledge = tiles;
        this.exploration_value = exploration_value;
    }

    public Constructeur() {
        super(null);
        type = Type_robot.Constructeur;
    }


}
