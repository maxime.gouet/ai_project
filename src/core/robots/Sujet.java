package core.robots;

import java.util.ArrayList;
/** Cette classe a était fait par Maxime Gouet*/
public abstract class Sujet {

    ArrayList<Observateur> listOfObservateurs;

    public void ajoute(Observateur obs){
        listOfObservateurs.add(obs);
    }
    public void retire(Observateur obs){
        listOfObservateurs.remove(obs);
    }
    public abstract void notifie();
}
