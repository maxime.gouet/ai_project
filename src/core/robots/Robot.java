package core.robots;

import core.deplacement.Strategie_deplacement;
import core.planet.Planet;
import core.planet.map.tiles.TType;
import core.planet.map.tiles.Tile;
import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;

import java.awt.*;
import java.util.ArrayList;

public abstract class Robot extends Sujet implements Observateur {
    protected Tile[][] knowledge;
    protected int[][] exploration_value;
    protected Point current_position;
    protected final WorkStrategy workStrategy;
    int size_of_tiles;
    Type_robot type;
    int health;
    Strategie_deplacement strategie;

    protected Robot(Point pos) {
        listOfObservateurs = new ArrayList<Observateur>();
        knowledge = null;
        size_of_tiles = 21;
        current_position = pos;
        health = 10;
        strategie = null;
        workStrategy = new WorkStrategy();
    }
    /** Cette method a était réaliser par Maxime Gouet */
    public void setKnowledge(Tile[][] tiles){
        this.knowledge = tiles;
        this.exploration_value = new int[size_of_tiles][size_of_tiles];
        for(int i = 0;i<size_of_tiles;i++){
            for(int y= 0;y<size_of_tiles;y++){
                exploration_value[i][y] = 101;
            }
        }
    }
    /** Cette method a était réaliser par Maxime Gouet */
    public void notifie() {
        for (Observateur obs : listOfObservateurs) {
            obs.actualise(knowledge,exploration_value);
        }
    }
    /** Cette method a était réaliser par Maxime Gouet */
    public void setStrategie(Strategie_deplacement str) {
        this.strategie = str;
    }

    public abstract void mooving();

    public Point getPosition() {
        return current_position;
    }

    public Type_robot getType() {
        return type;
    }

    /** Cette method a était en partie réaliser par Maxime Gouet
     * Devrait être ameliorer pour ne pas passer la planet entière en parametre.
     */
    public void capture(Planet planet){
        int x = current_position.x;
        int y = current_position.y;

        knowledge[x][y] = planet.getMap().getTiles()[x][y];
        exploration_value[x][y] = 0;

        if (x > 0) {
            knowledge[x - 1][y] = planet.getMap().getTiles()[x - 1][y];
            exploration_value[x - 1][y] = 0;
        }
        if (x < 20) {
            knowledge[x + 1][y] = planet.getMap().getTiles()[x + 1][y];
            exploration_value[x + 1][y] = 0;
        }
        if (y > 0) {
            knowledge[x][y - 1] = planet.getMap().getTiles()[x][y - 1];
            exploration_value[x][y-1] = 0;
        }
        if (y < 20){
            knowledge[x][y + 1] = planet.getMap().getTiles()[x][y + 1];
            exploration_value[x][y+1] = 0;
        }
        this.notifie();
    }

    /** Cette method a était réaliser par Olivier Lallinec */
    public int dysfunction(int metamorphose) {
        if (TType.Obstacle.equals(knowledge[current_position.x][current_position.y].getTType())) {
            return 0;
        } else {
            String fileName = "resources/fcl/dysfunction.fcl";
            FIS fis = FIS.load(fileName, true);
            if (fis == null) { // Error while loading?
                System.err.println("Can't load file: '" + fileName + "'");
                return health;
            }
            FunctionBlock functionBlock = fis.getFunctionBlock(null);

            functionBlock.setVariable("metamorphose", metamorphose);
            functionBlock.evaluate();
            double healthModifier = functionBlock.getVariable("Health").getValue();
            return (int) (health * (healthModifier / 100));
        }
    }
    public Point getCurrent_position(){
        return current_position;
    }
    public abstract void work(Tile tile);
}
