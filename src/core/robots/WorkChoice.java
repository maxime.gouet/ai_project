/* author: Lucas Guilbert */
package core.robots;

public enum WorkChoice {
    EXPLOITATION, EXPLORATION, RETURNING
}
