package core.robots;

import core.deplacement.A_star;
import core.planet.map.tiles.*;
import core.deplacement.Q_Learning;

import java.awt.*;

public class Extracteur extends Robot {
    private int oreStock;
    private WorkChoice currentTask;
    private boolean isExploiting;

    public Extracteur(Point pos) {
        super(pos);
        oreStock = 0;
        type = Type_robot.Extracteur;
        currentTask = workStrategy.whatDoIdo();
        isExploiting = currentTask == WorkChoice.EXPLOITATION;
        setStrategie(new Q_Learning());
    }
    /** Cette method a était réaliser par Maxime Gouet */
    public void actualise(Tile[][] tiles, int[][] exploration_value) {
        this.knowledge = tiles;
        this.exploration_value = exploration_value;
    }

    @Override
    /** Cette method a était réaliser par Maxime Gouet */
    public void mooving() {
        Point posToMoveTo = strategie.mooving(knowledge, exploration_value, current_position, this.getType(), currentTask);
        /*if (!(knowledge[current_position.x][current_position.y] instanceof TLake) &&
                !(knowledge[current_position.x][current_position.y] instanceof TObstacle)) {
            current_position = posToMoveTo;
        }*/
        if (posToMoveTo != null) current_position = posToMoveTo;
    }

    public Extracteur() {
        super(null);
        type = Type_robot.Extracteur;
    }

    /** Cette method a était réaliser par Olivier Lallinec */
    public void work(Tile tile) {
        // Define the new thing to do
        if (!isExploiting) {
            currentTask = workStrategy.whatDoIdo();
            isExploiting = currentTask == WorkChoice.EXPLOITATION;
        }

        if (isExploiting) {
            TBase base;
            if (tile.getTType() == TType.Ore && oreStock == 0) {
                ((TOre) tile).mineOre(2);
                oreStock = 2;
                ((TOre) knowledge[current_position.x][current_position.y]).setisExploited(true);
                this.setStrategie(new A_star());
            } else if (tile.getTType() == TType.Base && oreStock != 0) {
                base = (TBase) tile;
                oreStock = 0;
                this.setStrategie(new Q_Learning());
                isExploiting = false;
                base.addOre(2);
            }
        }
    }
}
