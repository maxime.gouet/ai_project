package core.robots;

import core.deplacement.Still;
import core.planet.Planet;
import core.planet.map.tiles.Tile;

import java.awt.*;

public class Centralisateur extends Robot{
    int stock_ores;
    int stock_water;
    int stock_food;

    public Centralisateur(Point pos) {
        super(pos);
        setStrategie(new Still());
    }

    @Override
    public void work(Tile tile) {

    }
    /** Cette method a était en partie réaliser par Maxime Gouet */
    @Override
    public void mooving() {
        current_position = strategie.mooving(knowledge,exploration_value, current_position, this.getType(), workStrategy.whatDoIdo());
    }
    /** Cette method a était réaliser par Maxime Gouet */
    public void actualise(Tile[][] tiles,int[][] exploration_value){
        this.knowledge = tiles;
        this.exploration_value = exploration_value;
        this.notifie();
    }
    /** Cette method a était en partie réaliser par Maxime Gouet */
    public void capture(Planet planet){
        for(int i = 0;i<size_of_tiles;i++){
            for(int y= 0;y<size_of_tiles;y++){
                if(i != current_position.x && y != current_position.y){
                    exploration_value[i][y] = exploration_value[i][y]+1;
                }
            }
        }
        notifie();
    }

    public Centralisateur() {
        super(null);
        this.stock_ores = 0;
        this.stock_water = 0;
        this.stock_food = 0;
        type = Type_robot.Centralisateur;
    }

    public Tile[][] getKnowledge() {
        return knowledge;
    }
}
